# Developer Guide for @fnet/files-to-aws-s3

## Overview

The `@fnet/files-to-aws-s3` library is designed to simplify the process of uploading files from your local system to an Amazon S3 bucket. By utilizing this library, developers can automate file uploads based on specified patterns, manage file destinations within S3, and define metadata for each uploaded file. The library is particularly useful for scenarios where files need to be programmatically transferred to S3, making it easier to handle batch uploads in a consistent and efficient manner.

## Installation

You can install the library using either npm or yarn. Here are the installation commands:

```bash
# Using npm
npm install @fnet/files-to-aws-s3

# Using yarn
yarn add @fnet/files-to-aws-s3
```

## Usage

To use the library, ensure that your AWS credentials (e.g., AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION) are properly set in your environment. Here’s a practical example of how to use this library to upload files to an S3 bucket:

```javascript
import uploadFilesToS3 from '@fnet/files-to-aws-s3';

(async () => {
  try {
    const result = await uploadFilesToS3({
      bucket: 'your-s3-bucket-name',
      dir: '/path/to/local/files',
      pattern: '**/*.txt', // pattern to match files
      destDir: '/desired/destination/on/s3',
      region: 'us-east-1',
      verbose: true // enable to log each upload
    });

    console.log('Upload results:', result);
  } catch (error) {
    console.error('Error uploading files:', error);
  }
})();
```

## Examples

Here are some typical use cases for this library:

### Example 1: Basic File Upload

```javascript
import uploadFilesToS3 from '@fnet/files-to-aws-s3';

uploadFilesToS3({
  bucket: 'example-bucket',
  pattern: '*.jpg',
  region: 'us-west-2'
}).then(result => {
  console.log('Files uploaded:', result.files);
}).catch(error => {
  console.error('Error:', error);
});
```

### Example 2: Upload with Metadata and Dry Run

```javascript
import uploadFilesToS3 from '@fnet/files-to-aws-s3';

uploadFilesToS3({
  bucket: 'example-bucket',
  pattern: '*.png',
  metadata: { author: 'your-name' },
  dryRun: true, // simulate upload without actually uploading
  region: 'eu-central-1'
}).then(result => {
  console.log('Simulated upload result:', result.files);
}).catch(error => {
  console.error('Error:', error);
});
```

## Acknowledgement

This library leverages `@aws-sdk/client-s3` for managing S3 interactions and `@fnet/list-files` for file pattern matching, seamlessly integrating these tools to provide a streamlined file uploading experience to AWS S3.

By following this guide, you should be equipped to efficiently use `@fnet/files-to-aws-s3` for your file uploading needs to AWS S3 with ease.