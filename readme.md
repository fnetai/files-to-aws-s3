# @fnet/files-to-aws-s3

This project provides a simple and straightforward utility for uploading files from a local directory to an Amazon S3 bucket. It is designed to facilitate the transfer of files that match a specific pattern, helping users manage and store their data in the cloud with ease.

## How It Works

The utility scans a specified directory for files that match a given pattern. It then uploads these files to a designated AWS S3 bucket. Users can specify the destination directory within the S3 bucket, and optionally run a "dry run" to preview uploads without actually executing them. This process is managed through AWS credentials and region configurations to ensure secure and accurate data handling.

## Key Features

- **Pattern-Based File Selection**: Specify a pattern to select files within a directory for upload.
- **Customizable Destination**: Choose a destination directory within your S3 bucket.
- **Metadata Management**: Attach custom metadata to each file being uploaded.
- **Dry Run Mode**: Preview which files would be uploaded without executing the upload.
- **Verbose Output Option**: Enable verbose mode to log details of the upload process.

## Conclusion

This utility offers a modest and practical solution for users needing to transfer files to AWS S3. It is designed to streamline the process with its pattern matching, customizable settings, and secure handling through AWS.