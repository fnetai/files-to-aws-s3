import path from "node:path";
import mime from "mime-types";
import fs from "node:fs";

import { S3Client, PutObjectCommand } from "@aws-sdk/client-s3";
import { fromNodeProviderChain } from "@aws-sdk/credential-providers";

import listFiles from "@fnet/list-files";

// @howto: It assumes some AWS environment variables are set, like AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_REGION in process environment \n
// @howto: Developer should know required AWS environment variables and set them before running this function \n
export default async ({
  bucket,
  dir = process.cwd(),
  pattern,
  destDir = "/",
  dryRun = false,
  metadata = {},
  region,
  verbose = false
}) => {

  if (!bucket) throw new Error('bucketName is required.');
  if (!pattern) throw new Error('pattern is required.');

  const s3 = new S3Client({
    credentials: fromNodeProviderChain(),
    region
  });

  const files = await listFiles({ pattern, dir, nodir: true });

  const uploadedFiles = [];

  for await (const file of files) {
    const srcFile = path.resolve(dir, file);
    const destFile = path.join(destDir, file).replace(/\\/g, "/");

    let defaultUrl = `https://${bucket}.s3.${region}.amazonaws.com/${destFile}`;

    const uploadedFile = { from: srcFile, to: destFile, url: defaultUrl };
    uploadedFiles.push(uploadedFile);

    const metadataFinal = {
      'x-amz-meta-surrogate-key': "s3",
      ContentType: mime.contentType(path.extname(file)) || 'application/octet-stream',
      ...metadata
    };

    if (!dryRun) {
      try {
        const fileStream = fs.createReadStream(srcFile);
        await s3.send(new PutObjectCommand({
          Bucket: bucket,
          Key: destFile,
          Body: fileStream,
          ContentType: metadataFinal.ContentType,
          Metadata: metadataFinal
        }));
      } catch (error) {
        // console.error(`Error uploading file ${srcFile}:`, error);
        throw new Error(`Error uploading file ${srcFile}: ${error.message}`);
      }
    }

    if (verbose) {
      console.log(`Uploaded: ${srcFile} to ${defaultUrl}`);
    }
  }

  return {
    files: uploadedFiles
  };
};